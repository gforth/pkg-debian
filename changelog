gforth (0.7.2+dfsg1-1) unstable; urgency=medium

  * Update the copyright file a bit:
    - convert it to the 1.0 format
    - correct some of the copyright notices
    - bump the year of my copyright notice
  * Bump Standards-Version to 3.9.5:
    - remove the dependency on dpkg (>= 1.15.4) | install-info
  * Update the debian/patch/* descriptions a bit:
    - add the 'Description' tag to the 06-configure-assumptions one
    - shorten the description of the 07-manpage one
  * Update the list of build dependencies:
    - actually depend on debhelper 9, not 8.1.3, and remove the now
      outdated debian/source.lintian-overrides file
    - remove the dpkg-dev dependency - the version requirement is
      satisfied even in oldstable
  * Use the hardening flags supplied by debhelper, ask for everything
    except PIE.
  * Remove the debian/source/options file, dpkg-dev's defaults for
    compressing the source and binary packages are good enough.
  * Use dh-autoreconf instead of autotools-dev and retarget
    the 06-configure-assumptions patch.
  * New upstream release:
    - Closes: #726438 (New GNU Forth version available)
    - Closes: #705432 (Please update to 0.7.2)
    - no more CVS directories, so no need to repack the source
    - on second thoughts, repack the source, acknowledging David Prévot's
      NMU (thanks!): remove the non-DFSG documentation.  I will create
      a separate non-free package for that.
    - drop the 10-engine-subst.patch, included upstream
    - drop the 11-static-newline patch, included upstream, and the rules
      file snippet that saves and restores prim.b
    - adapt the 06-configure-assumptions patch
    - refresh the 02-skip-install patch (line numbers only)
    - update the copyright years
    - remove the build dependency on the GForth interpreter introduced
      as a temporary workaround in 0.7.0+ds1-7
  * Let uscan verify the upstream PGP signature against M. Anton Ertl's
    and Bernd Paysan's keys.
  * Update the 02-skip-install patch to not even try to byte-compile
    the gforth.el file - it will be handled correctly at install time.
  * Add the 12-elisp-byte-compile patch to fix the byte compilation with
    Emacs 24 - remove the eval-when-compile wrapper.  Closes: #685090
  * Actually add forth-mode to the autodetected modes at Emacs startup.
    Closes: #587318 (this time for real).
  * Update the Emacs install/remove infrastructure a bit and let gforth
    depend on emacsen-common 2.0.7.  Note to self: update this to 2.0.8
    once it hits the archive.

 -- Peter Pentchev <roam@ringlet.net>  Tue, 18 Mar 2014 13:08:07 +0200

gforth (0.7.0+ds2-0.1) unstable; urgency=low

  * Non-maintainer upload.
  * Remove non DFSG-compliant doc/gforth.ds, doc/gforth.info*, doc/gforth.ps,
    doc/vmgen.info, doc/vmgen.ps, and doc/vmgen.texi from source. The gforth
    binary package doesn't ship the /usr/share/info doc anymore because of
    that. Closes: #695724

 -- David Prévot <taffit@debian.org>  Sun, 30 Dec 2012 13:17:23 -0400

gforth (0.7.0+ds1-7) unstable; urgency=low

  * Apply an upstream patch to fix a FTBFS; Closes: #672616
    - add the 11-static-newline patch to define the system newline
      string as static
    - temporarily build-depend on the GForth interpreter to be able to
      rebuild the kernel images properly after applying the patch
    - stash and restore the generated files affected by the patch

 -- Peter Pentchev <roam@ringlet.net>  Tue, 04 Sep 2012 17:52:59 +0300

gforth (0.7.0+ds1-6) unstable; urgency=low

  * Fix the watch file syntax so the +ds1 part is properly stripped off.
  * Convert to the 3.0 (quilt) format.
  * Make gforth.el register the .fs, .4th, and .fth filename suffixes.
    Closes: #587318
  * Bump Standards-Version to 3.9.2:
    - gforth-common now Breaks older gforth versions, no longer Conflicts
      with them
  * Use dpkg-buildflags from dpkg-dev 1.15.7 to obtain the default values
    for CFLAGS, CPPFLAGS, and LDFLAGS.
  * Update the copyright file:
    - bring it up to the latest version of the DEP 5 format
    - use the proper GFDL-NIV short license name instead of just GFDL,
      as spotted by Nicholas Bamber (periapt)
    - fix the DEP 5 URL after the Alioth migration
    - bump the year on my copyright notice
  * Use the debhelper plugin provided by autotools-dev >= 20100122.1 to
    refresh the config.sub and config.guess files.
  * Also byte-compile the emacs mode for emacs-snapshot.  Closes: #615142
  * Acknowledge the NMU; thanks, Luk!
  * libffcall1 seems to build fine on armel now, so use it everywhere.
  * Bump the debhelper compatibility level to 9:
    - reorder the dh helper arguments to place the sequence name first
    - use the still not-quite-finalized level 9 instead of the recommended 8
      because of multiarch support in dh_auto_configure
  * Switch to bzip2 compression for the Debian tarball.
  * Convert to multiarch:
    - move /usr/lib/*/gforth/ to a new gforth-lib package and mark it as
      Multi-Arch: same
    - mark gforth and gforth-common as Multi-Arch: foreign
    - add the 10-engine-subst patch to fix the libdir handling in engine/
    - use debhelper compatibility level 9 for dh_auto_configure
  * Override some more Lintian warnings:
    - more "unusual interpreter" for GForth and its kernel
    - no debhelper 9 dependency yet, the compatibility level is for
      multiarch only
    - add some comments explaining the overrides as suggested by
      Nicholas Bamber (periapt)
  * Move the Debian packaging to Gitorious and update the Vcs-Git and
    Vcs-Browser URLs.
  * Add DEP-3 headers to the rest of the patches as prompted by
    Nicholas Bamber (periapt)

 -- Peter Pentchev <roam@ringlet.net>  Mon, 18 Jul 2011 17:51:22 +0300

gforth (0.7.0+ds1-5.1) unstable; urgency=low

  * Non-maintainer upload.
  * Don't ship .la files (Closes: #621255).

 -- Luk Claes <luk@debian.org>  Mon, 13 Jun 2011 17:56:27 +0200

gforth (0.7.0+ds1-5) unstable; urgency=low

  * Fix the FTBFS on hppa by not passing -N to the linker.
    Many thanks to Frans Pop and the others on the debian-hppa list for
    the assistance in tracking this down!

 -- Peter Pentchev <roam@ringlet.net>  Tue, 08 Sep 2009 16:21:38 +0200

gforth (0.7.0+ds1-4) unstable; urgency=low

  * Try and fix the s390 build once again, this time after testing
    the sdiv_qrnnd() usage code on x86; hopefully Closes: #544827

 -- Peter Pentchev <roam@ringlet.net>  Sat, 05 Sep 2009 18:46:33 +0300

gforth (0.7.0+ds1-3) unstable; urgency=low

  * Explicitly depend on libtool and libltdl-dev; several benefits:
    - make the build environment-agnostic and not dependent on what
      just happens to be installed
    - fix a FTBFS if libtool is installed but libltdl-dev isn't;
      some of the autobuilders are configured that way
  * Add a missing semicolon in engine/support.c to try and fix
    the FTBFS on s390.
  * Temporarily use libffi instead of libffcall on armel, until
    libffcall's issues are resolved.

 -- Peter Pentchev <roam@ringlet.net>  Tue, 01 Sep 2009 17:58:12 +0300

gforth (0.7.0+ds1-2) unstable; urgency=low

  * Fix the FTBFS when building arch-dependent packages only - do not
    depend on dh_installdirs to create the gforth-common /usr/share
    directory, since it won't in an arch-only build.  Closes: #543569

 -- Peter Pentchev <roam@ringlet.net>  Wed, 26 Aug 2009 15:03:13 +0300

gforth (0.7.0+ds1-1) unstable; urgency=low

  * New maintainer.  Closes: #540827
  * Use quilt for patch management and add the debian/README.source file.
  * Fix the build:
    - specify an absolute prefix to "make install".
    - remove a couple of files that "make distclean" seems to miss.
  * Add the Homepage, Vcs-Svn, and Vcs-Browser control fields.
  * Install the NEWS file as an upstream changelog.
  * Deal with config.sub and config.guess properly - copy them from
    the autotools-dev package before running "configure" and remove them
    in the "clean" target.
  * Keep the vmgen.1 manual page in the debian/ directory.
  * Bump the debhelper compatibility level to 7:
    - version the debhelper build dependency
    - add ${misc:Depends} to the binary package
    - leave removing of the *-stamp files to dh_clean
    - use dh_prep instead of dh_clean -k
    - shorten the rules file using the dh(1) helper and override targets
    - disable debhelper's verbose mode
  * Bump Standards-Version to 3.8.3:
    - leave "install-info" to dpkg triggers and add the proper dependency
  * Disable an i*86 "optimization" of passing the nonexistent -m486
    option to gcc.
  * Break out /usr/share/gforth/ into a gforth-common package.
  * Make three example scripts executable.
  * Repackage to remove the CVS directories.
  * Fix the .TQ macro invocations in the groff.1 manual page so that
    the short options are actually rendered!
  * Add all the copyright holders to the copyright file and convert it
    to the DEP 5 format.
  * Properly compile and install gforth.el.  Closes: #385399.

 -- Peter Pentchev <roam@ringlet.net>  Sun, 23 Aug 2009 14:20:28 +0300

gforth (0.7.0-0.1) unstable; urgency=low

  * Non-maintainer upload.
  * New upstream version which is able to build on systems with address-space
    randomization, closes: #484222
  * Build with ffcall support, closes: #500640
  * Applied upstream patch to not accept "-" as a valid number, closes: #512364
  * Added watch file.
  * Bumped Standards-Version to 3.8.1, no changes needed.

 -- Michael Meskes <meskes@debian.org>  Mon, 20 Apr 2009 14:28:05 +0200

gforth (0.6.2-7.3) unstable; urgency=low

  * Non-maintainer upload.
  * Removed -s option from install, closes: #436976
  * Removed deprecated gcc options, closes: #474806
  * Bumped Standards-Version to 3.7.3.
  * Changed build dependency from automake1.4 to automake.
  * Bumped compatibility level to 4.

 -- Michael Meskes <meskes@debian.org>  Wed, 23 Apr 2008 13:13:53 +0200

gforth (0.6.2-7.2) unstable; urgency=high

  * Non-maintainer upload.
  * Fix breackage on hppa based on the comments from Lamont.
    Closes: #389236

 -- Andreas Barth <aba@not.so.argh.org>  Tue, 19 Dec 2006 16:48:45 +0000

gforth (0.6.2-7.1) unstable; urgency=low

  * Non-maintainer upload.
  * gforth_0.6.2-7(hppa/unstable): FTBFS: bad build-depends 
  (Closes: #394407)

 -- Neil Williams <codehelp@debian.org>  Fri, 27 Oct 2006 19:19:01 +0100

gforth (0.6.2-7) unstable; urgency=low

  * Updating config.{guess,sub} (Closes: #342411)

 -- Eric Schwartz (Skif) <emschwar@debian.org>  Tue, 14 Feb 2006 08:58:57 -0700

gforth (0.6.2-6) unstable; urgency=low

  * Fix buggy postinst (Closes: #289571)

 -- Eric Schwartz (Skif) <emschwar@debian.org>  Sun,  9 Jan 2005 18:24:53 -0700

gforth (0.6.2-5) unstable; urgency=low

  * install-info vmgen info page (closes: #268714)
  * clarify wording in gforth man page (closes: #278325)

 -- Eric Schwartz (Skif) <emschwar@debian.org>  Sun,  9 Jan 2005 00:04:04 -0700

gforth (0.6.2-4) unstable; urgency=low

  * Add build-dep on automake (Closes: #245317)

 -- Eric Schwartz (Skif) <emschwar@debian.org>  Mon, 26 Apr 2004 14:41:16 -0600

gforth (0.6.2-3) unstable; urgency=low

  * Include LaMont's patch to enable big-endian and 64-bit
    builds.  (Closes: #221861)
  * Fixed configure.in to create valid assembler to reserve 16 bytes
    of space (Closes: #188513)

 -- Eric Schwartz (Skif) <emschwar@debian.org>  Sun, 11 Apr 2004 17:00:55 -0600

gforth (0.6.2-2.2) unstable; urgency=low

  * NMU
  * Sigh. Fix 64-bit machine builds too. (Really) Closes: #221861

 -- LaMont Jones <lamont@ia.mmjgroup.com>  Wed, 24 Dec 2003 16:07:07 -0700

gforth (0.6.2-2.1) unstable; urgency=low

  * NMU
  * Fix big-endian machine build.  Closes: #221861

 -- LaMont Jones <lamont@debian.org>  Tue, 23 Dec 2003 21:20:08 -0700

gforth (0.6.2-2) unstable; urgency=low

  * Cleaned up some lintian problems.  Nothing major.

 -- Eric Schwartz <emschwar@debian.org>  Thu, 20 Nov 2003 00:40:02 -0700

gforth (0.6.2-1) unstable; urgency=low

  * New upstream release
  * Remove INSTALL_INFO from Makefile.in to not generate
    /usr/share/doc/info/dir.(old).gz (closes: #213680)

 -- Eric Schwartz <emschwar@debian.org>  Tue, 14 Oct 2003 21:43:02 -0600

gforth (0.6.1-4) unstable; urgency=low

  * set no_dynamic_default=1 as a quick fix from the gforth mailing list.
    (closes: #195364)
  
  * Applied Mario Lang's patch to autoload forth-block-mode and run-forth
    (closes: #198055)

 -- Eric Schwartz <emschwar@debian.org>  Tue,  5 Aug 2003 20:50:47 -0600

gforth (0.6.1-3) unstable; urgency=low

  * Added no_dynamic_default=1 for m68k in configure.in (closes #188270)

 -- Eric Schwartz <emschwar@debian.org>  Thu, 10 Apr 2003 10:09:40 -0600

gforth (0.6.1-2) unstable; urgency=low

  * Added autoconf to build-deps, removed gcc-3.2 (closes #188101)

 -- Eric Schwartz <emschwar@debian.org>  Mon,  7 Apr 2003 18:34:33 -0600

gforth (0.6.1-1) unstable; urgency=low

  * New upstream version

 -- Eric Schwartz <emschwar@debian.org>  Tue, 25 Mar 2003 13:12:03 -0700

gforth (0.5.0-6) unstable; urgency=low

  * Updates config.{sub,guess} (closes #176518)
  * Updates build options for m68k.  Again.  Dagnabit.

 -- Eric Schwartz <emschwar@debian.org>  Fri, 31 Jan 2003 18:14:55 -0700

gforth (0.5.0-5) unstable; urgency=low

  * Fixes won't-build-on-sparc problem

 -- Eric Schwartz <emschwar@debian.org>  Wed,  8 Jan 2003 17:07:47 -0700

gforth (0.5.0-4) unstable; urgency=low

  * Fixes incorrect build using --enable-force-regs on non-m68k archs

 -- Eric Schwartz <emschwar@debian.org>  Wed,  8 Jan 2003 10:15:40 -0700

gforth (0.5.0-3) unstable; urgency=low

  * Added portability patches from Anton Ertl (closes: #140153)
  * Forced compile on m68k to use --enable-force-reg (closes: #138894)
  * changed 50gforth.el to provide 'forth-mode (closes: #154042)
  * Applied Kevin Ryde's patch to gforth.el (closes: #139843)

 -- Eric Schwartz <emschwar@debian.org>  Mon,  6 Jan 2003 17:20:15 -0700

gforth (0.5.0-2) unstable; urgency=low

  * Incorporate Colin Watson's patch
  * Make /etc/emacs/site-start.d/50gforth.el a conffile (closes: #132136).

 -- Eric Schwartz <emschwar@debian.org>  Mon, 25 Feb 2002 10:55:55 -0700

gforth (0.5.0-1) unstable; urgency=low

  * new upstream version
  * update to current policy

 -- Eric M. Schwartz <emschwar@debian.org>  Sat, 30 Sep 2000 14:21:00 -0600

gforth (0.4.9.19990617-2) unstable; urgency=low

  * new maintainer

 -- Eric M. Schwartz <emschwar@debian.org>  Wed, 23 Aug 2000 22:01:00 -0600

gforth (0.4.9.19990617-1) unstable; urgency=low

  * new upstream source snapshot, not an official release but recommended by
    one of the upstream maintainers, Bernd Paysan
  * update to current policy

 -- Bdale Garbee <bdale@gag.com>  Wed,  1 Dec 1999 23:55:19 -0700

gforth (0.4.0-4) unstable; urgency=low

  * upstream source has CVS directories, which cvs-inject can't deal with

 -- Bdale Garbee <bdale@gag.com>  Sun, 20 Jun 1999 14:27:00 -0600

gforth (0.4.0-3) unstable; urgency=low

  * revert to upstream naming for installed executables, tweaking Makefile.in 
    to get the symlinks right.  Closes 34921.
  * tweak install.TAGS in Makefile.in so that it strips the build path from 
    the entries in the TAGS file properly.  Closes 34920.
  * add link for gforthmi man page

 -- Bdale Garbee <bdale@gag.com>  Wed, 26 May 1999 12:40:37 -0600

gforth (0.4.0-2) unstable; urgency=low

  * fix an include problem on m68k reported by Roman Hodek, closes 32738

 -- Bdale Garbee <bdale@gag.com>  Sat,  6 Feb 1999 16:34:15 -0700

gforth (0.4.0-1) unstable; urgency=low

  * new upstream version, closes 31563

 -- Bdale Garbee <bdale@gag.com>  Sun, 17 Jan 1999 23:28:14 -0700

gforth (0.3.0-4) unstable; urgency=low

  * m68k doesn't want -traditional-cpp per James Troup, closes 22537.
  * fix a couple of lintian complaints

 -- Bdale Garbee <bdale@gag.com>  Sun, 24 May 1998 22:37:06 -0600

gforth (0.3.0-3) frozen unstable; urgency=low

  * fix reference to etags.el in gforth.el, closes 20152
  * restructure 50gforth.el so it's loaded more intelligently, closes 18738.
  * switch to egcc, since current Debian gcc package causes hang during build.
    This problem is known to the gforth maintainers, they indicate it's a gcc
    problem but provide no details.  Since egcc works...  I'll just use it.
  * move from debstd to debhelper

 -- Bdale Garbee <bdale@gag.com>  Sat, 18 Apr 1998 01:48:32 -0600

gforth (0.3.0-2) unstable; urgency=low

  * libc6

 -- Bdale Garbee <bdale@gag.com>  Thu,  4 Sep 1997 23:45:47 -0600

gforth (0.3.0-1) unstable; urgency=low

  * New upstream elease.

 -- Bdale Garbee <bdale@gag.com>  Fri, 18 Apr 1997 21:44:03 -0600

gforth (0.2.1-1) unstable; urgency=low

  * New upstream version.

 -- Bdale Garbee <bdale@gag.com>  Fri, 20 Dec 1996 22:43:35 -0700

gforth (0.2.0-1) stable unstable; urgency=medium

  * New upstream version.
  * Fixes bugs 5973 and 5976.

 -- Bdale Garbee <bdale@gag.com>  Sat, 14 Dec 1996 14:54:37 -0700

gforth (0.1beta-3) unstable; urgency=low

  * Guy moved gforth to devel, closes bug 5301 - not re-released

 -- Bdale Garbee <bdale@gag.com>  Tue, 12 Nov 1996 09:21:34 -0700

gforth (0.1beta-2) unstable; urgency=low

  * Add postinst and postrm to make info files visible.

 -- Bdale Garbee <bdale@gag.com>  Sat,  2 Nov 1996 15:23:19 -0700

gforth (0.1beta-1) unstable; urgency=low

  * Initial Release.

 -- Bdale Garbee <bdale@gag.com>  Tue, 29 Oct 1996 02:03:02 -0800


